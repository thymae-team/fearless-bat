============
Fearless Bat
============

Fearless bat is nothing but a permission management service. Its only purpose to
determine which permissions certain user has and to which groups he belongs.

.. contents:: Table of contents
  :depth: 3
 
Purpose
-------

Decouple users from their permissions so we can easily modify permissions, roles and
groups without touching users' personal data.

Everything fearless bat knows about the user is their id and roles, groups, permissions.
This concept allows to keep data separated and reuse it.

Concept
-------

Entities
~~~~~~~~

Users
^^^^^

Users are represented in system as a unique identifier either UUID or Integer (to be discussed)

Roles
^^^^^

Roles are used to easily manage permissions of several users at the same time. Multiple users
can have the same role as well as one user can have multiple roles.

Role contains the following information about itself:

- title
- description

Permissions
^^^^^^^^^^^

Permissions are the essence of the service. 
Under the hood they are just labeled ids, so any client application
can implement its own logic based on them.

Permissions can be granted to

- Individual users
- Roles

If the permission is granted to the role, it is automatically granted to all users, which have that role.

Permissions contain the following information about themselves:

- title
- hint

Interactions
~~~~~~~~~~~~

Protocol
^^^^^^^^

As we have a strongly typed data in the interactions we can use a `gRPC`_ protocol with `ProtoBuf`_

Data retrival
^^^^^^^^^^^^^

We can request the following things:

- roles
  - list of all roles
  - list of all roles assigned to specific user
- permissions
  - list of all permissions
  - list of user's permissions
  - list of role's permissions
- checks
  - does user have a specified permission

Data modifications
^^^^^^^^^^^^^^^^^^

- entity management

  - role

    - create
    - modify
    - delete

  - permission

    - create
    - modify
    - delete
- intents

  - role

    - assign a role to a user
    - resign a role to a user

  - permission

    - grant permission to a user
    - revoke permission from user
    - grant permission to a role
    - revoke permission from role

.. _`gRPC`: http://grpc.io
.. _`ProtoBuf`: https://developers.google.com/protocol-buffers/
