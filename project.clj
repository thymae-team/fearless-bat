(defproject fearless-bat "0.1.0"
  :description "Permission management service"
  :url "https://bitbucket.org/thymae-team/fearless-bat"
  :license {:name "The MIT License"
            :url "http://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.8.0"]])
